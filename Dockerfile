FROM alpine:3.15.4@sha256:4edbd2beb5f78b1014028f4fbb99f3237d9561100b6881aabbf5acce2c4f9454 as builder
# hadolint ignore=DL3018,DL3019
RUN set -eux; \
        apk add alpine-sdk sudo; \
        abuild-keygen -a -i -n;
COPY / /abuild
WORKDIR /abuild
RUN set -eux; \
        abuild -F -r -P /abuild; \
        find . -name '*.apk' -type f -exec apk add --no-cache {} \;;

FROM scratch
COPY --from=builder /var/www/html/plugins/ /var/www/html/plugins
