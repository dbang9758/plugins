#!/usr/bin/env bash

set -eu

log() {
  local message="${1}"
  local level="${2:-info}"
  local context="${3:-main}"
  local currentDate
  currentDate="$(date --iso-8601=seconds)"
  echo "[${currentDate}] ${level^^} ${context}: ${message}"
}

setDefaults() {
  export GLPI_VERSIONS=("9.4" "9.5")
  export CONTAINERS=("alpine:3.15.0")
}

checkDocker() {
  local status=0
  docker info || status=$?
  if [[ "${status}" -ne 0 ]]; then
    log 'Docker is not ready' 'error'
  fi
  return $status
}

pullContainers() {
  log 'Pulling container images'
  for container in "${CONTAINERS[@]}"; do
    docker pull "${container}"
  done
}

buildPlugins() {
  log 'Building plugins'
  local plugins=()
  local pluginNames=("$@")
  if [[ "${#pluginNames[@]}" -eq 0 ]]; then
    local version
    for version in "${GLPI_VERSIONS[@]}"; do
      readarray -d '' -O "${#plugins[@]}" -t plugins < <(find "./${version}" -mindepth 1 -maxdepth 1 -type d -print0)
    done
  else
    local version
    for version in "${GLPI_VERSIONS[@]}"; do
      local plugin
      for plugin in "${pluginNames[@]}"; do
        if [[ -d "./${version}/${plugin}" ]]; then
          plugins+=("./${version}/${plugin}")
        fi
      done
    done
  fi
  if [[ "${#plugins[@]}" -eq 0 ]]; then
    log 'Nothing to build'
    return 0
  fi
  local plugin
  for plugin in "${plugins[@]}"; do
    local glpiVersion
    glpiVersion=$(echo "${plugin}" | cut -d '/' -f2)
    local pluginName
    pluginName=$(echo "${plugin}" | cut -d '/' -f3)
    log "Building ${pluginName} for GLPI ${glpiVersion}"
    docker build \
      -f Dockerfile \
      -t "registry.gitlab.com/akontainers/glpi-plugins/${glpiVersion}/${pluginName}:latest" \
      "${plugin}"
  done
}

main() {
  setDefaults
  checkDocker
  pullContainers
  buildPlugins "$@"
}

main "$@"
