#!/usr/bin/env sh
set -eu

plugins=$(find "./${VERSION}" -mindepth 1 -maxdepth 1 -type d -exec basename {} \;)

cat << YML
---
include:
  - local: /.gitlab/ci/templates/plugin.gitlab-ci.yml

Stub:
  extends: .stub_template
  rules:
    - changes:
        - "${VERSION}/*/APKBUILD"
      when: never
    - when: on_success
YML

for plugin in ${plugins}; do
  plugin_version=$(grep ^pkgver "./${VERSION}/${plugin}/APKBUILD" | cut -d= -f2)
  cat << YML

Lint ${VERSION}:${plugin} APKBUILD:
  extends: .apkbuild_lint_template
  variables:
    APKBUILD_PATH: "${VERSION}/${plugin}/APKBUILD"
  rules:
    - if: \$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH
      when: never
    - changes:
        - "${VERSION}/${plugin}/APKBUILD"

Build ${VERSION}:${plugin}:
  extends: .build_plugin_template
  variables:
    GLPI_VERSION: "${VERSION}"
    PLUGIN_NAME: "${plugin}"
    CONTAINER_TAG: "${plugin_version}"
  rules:
    - if: \$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH
      when: never
    - changes:
        - "${VERSION}/${plugin}/APKBUILD"

Build and push ${VERSION}:${plugin}:
  extends: .build_and_push_plugin_template
  variables:
    GLPI_VERSION: "${VERSION}"
    PLUGIN_NAME: "${plugin}"
    CONTAINER_TAG: "${plugin_version}"
  rules:
    - if: \$CI_COMMIT_BRANCH != \$CI_DEFAULT_BRANCH
      when: never
    - changes:
        - "${VERSION}/${plugin}/APKBUILD"

YML
done
